package pcd.ass03.ex3;

public interface TemperatureSensor {

    double getCurrentValue();

}
