package pcd.ass03.ex3;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.flowables.ConnectableFlowable;
import pcd.ass03.ex3.acme.TemperatureSensorA1;

public class ObservableTemperatureSensorA1 extends TemperatureSensorA1 implements ObservableTemperatureSensor {

    private double speed;

    public ObservableTemperatureSensorA1(final double speed) {
        this.speed = speed;
    }

    @Override
    public Flowable<Double> createObservable() {
        Flowable<Double> source = Flowable.create((emitter) -> {
            new Thread(() -> {
                while (true) {
                    emitter.onNext(this.getCurrentValue());
                    try {
                        Thread.sleep((long) this.speed);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }, BackpressureStrategy.BUFFER);

        ConnectableFlowable<Double> hotObservable = source.publish();
        hotObservable.connect();
        return hotObservable;
    }

}
