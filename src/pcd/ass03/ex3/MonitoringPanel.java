package pcd.ass03.ex3;

import javax.swing.*;
import java.awt.*;

public class MonitoringPanel extends JFrame {

    private JPanel panel1;
    private GridBagConstraints c = new GridBagConstraints();

    public MonitoringPanel(final OutputPrinter... outputPrinters) throws HeadlessException {
        super();
        this.panel1 = new JPanel(new GridBagLayout());
        this.getContentPane().add(this.panel1);
        this.c.insets = new Insets(3, 3, 3, 3);

        this.c.gridx = 0;
        this.c.gridy = 0;

        this.panel1.add(new JLabel("RoomA"), this.c);
        this.c.gridy++;
        JScrollPane scrollPane0 = new JScrollPane((OutputPrinterArea) outputPrinters[0], JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        this.panel1.add(scrollPane0, this.c);
        this.c.gridy = 0;
        this.c.gridx++;

        this.panel1.add(new JLabel("RoomB1"), this.c);
        this.c.gridy++;
        JScrollPane scrollPane1 = new JScrollPane((OutputPrinterArea) outputPrinters[1], JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        this.panel1.add(scrollPane1, this.c);
        this.c.gridy = 0;
        this.c.gridx++;

        this.panel1.add(new JLabel("RoomB2"), this.c);
        this.c.gridy++;
        JScrollPane scrollPane2 = new JScrollPane((OutputPrinterArea) outputPrinters[2], JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        this.panel1.add(scrollPane2, this.c);

        this.c.gridx--;
        this.c.gridy++;
        this.panel1.add((OutputPrinterLabel) outputPrinters[3], this.c);

        this.setVisible(true);
        this.setResizable(false);
        this.setSize(700, 500);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }

}
