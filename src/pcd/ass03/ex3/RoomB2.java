package pcd.ass03.ex3;

import io.reactivex.Flowable;
import pcd.ass03.ex3.acme.TemperatureSensorB2;

public class RoomB2 implements Room {

    private TemperatureSensorB2 temperatureSensor;

    public RoomB2() {
        this.temperatureSensor = new TemperatureSensorB2();
    }

    @Override
    public Flowable<Double> getObservable() {
        return this.temperatureSensor.createObservable();
    }

}
