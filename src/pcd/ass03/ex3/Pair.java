package pcd.ass03.ex3;

public class Pair<L, R> {

    private L left;
    private R right;

    public Pair(final L left, final R right) {
        this.left = left;
        this.right = right;
    }

    public L getLeft() {
        return this.left;
    }

    public void setLeft(final L left) {
        this.left = left;
    }

    public R getRight() {
        return this.right;
    }

    public void setRight(final R right) {
        this.right = right;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Pair))
            return false;
        Pair<L, R> pairo = (Pair<L, R>) o;
        return this.left.equals(pairo.getLeft()) && this.right.equals(pairo.getRight());
    }

    public String toString() {
        return "( b1: " + this.left + ", b2: " + this.right + " )";
    }

}
