package pcd.ass03.ex3;

import io.reactivex.Flowable;

public class RoomA1 implements Room {

    private ObservableTemperatureSensorA1 temperatureSensor;

    public RoomA1() {
        this.temperatureSensor = new ObservableTemperatureSensorA1(500);
    }

    @Override
    public Flowable<Double> getObservable() {
        return this.temperatureSensor.createObservable();
    }
}
