package pcd.ass03.ex3;

import io.reactivex.Flowable;

public interface Room {

    Flowable<Double> getObservable();

}
