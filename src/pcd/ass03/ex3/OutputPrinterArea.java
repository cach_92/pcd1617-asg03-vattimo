package pcd.ass03.ex3;

import javax.swing.*;
import javax.swing.text.DefaultCaret;

public class OutputPrinterArea extends JTextArea implements OutputPrinter {

    /**
     * Constructs a new TextArea.  A default model is set, the initial string
     * is null, and rows/columns are set to 0.
     */
    public OutputPrinterArea() {
        this.setColumns(15);
        this.setRows(20);
        this.setEditable(false);
        ((DefaultCaret) this.getCaret()).setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
    }

    @Override
    public void print(final String output) {
        this.setText(this.getText() + "\n" + output);
    }

}
