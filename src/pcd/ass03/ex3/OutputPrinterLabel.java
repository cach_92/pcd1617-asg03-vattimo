package pcd.ass03.ex3;

import javax.swing.*;
import java.awt.*;

public class OutputPrinterLabel extends JLabel implements OutputPrinter {

    public OutputPrinterLabel() {
        super();
        this.setForeground(Color.RED);
    }

    @Override
    public void print(final String output) {
        this.setText(output);
    }

}
