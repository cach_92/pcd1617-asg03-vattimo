package pcd.ass03.ex3;

import io.reactivex.Flowable;
import pcd.ass03.ex3.acme.TemperatureSensorB1;

public class RoomB1 implements Room {

    private TemperatureSensorB1 temperatureSensor;

    public RoomB1() {
        this.temperatureSensor = new TemperatureSensorB1();
    }

    @Override
    public Flowable<Double> getObservable() {
        return this.temperatureSensor.createObservable();
    }

}
