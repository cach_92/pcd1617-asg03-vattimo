package pcd.ass03.ex3;

import io.reactivex.Flowable;

public class RoomMonitoring {

    private static final double THRESHOLD = 20;

    public static void main(final String[] args) {

        OutputPrinter outputPrinterA1 = new OutputPrinterArea();
        OutputPrinter outputPrinterB1 = new OutputPrinterArea();
        OutputPrinter outputPrinterB2 = new OutputPrinterArea();
        OutputPrinter outputPrinterAlarm = new OutputPrinterLabel();

        new MonitoringPanel(outputPrinterA1, outputPrinterB1, outputPrinterB2, outputPrinterAlarm);

        RoomA1 roomA1 = new RoomA1();
        Room roomB1 = new RoomB1();
        Room roomB2 = new RoomB2();

        Flowable<Double> flowA1 = roomA1.getObservable().filter(el -> isInRange(el));
        Flowable<Double> flowB1 = roomB1.getObservable().filter(el -> isInRange(el));
        Flowable<Double> flowB2 = roomB2.getObservable().filter(el -> isInRange(el));

        flowB1.subscribe(el -> outputPrinterB1.print(String.valueOf(el)));
        flowB2.subscribe(el -> outputPrinterB2.print(String.valueOf(el)));

        Flowable.combineLatest(flowB1, flowB2, (aDouble, aDouble2) -> new Pair<>(aDouble, aDouble2))
//                .filter(pair -> pair.getLeft() > THRESHOLD && pair.getRight() > THRESHOLD)
                .subscribe(c -> {
                    if (c.getLeft() > THRESHOLD && c.getRight() > THRESHOLD) {
                        outputPrinterAlarm.print("ALARM");
                        System.out.println("ALARM");
                    } else {
                        outputPrinterAlarm.print("");
                    }
                });

        flowA1.subscribe(obs -> {
            System.out.println("a1: " + obs);
            outputPrinterA1.print(String.valueOf(obs));
        });
    }

    private static boolean isInRange(final double value) {
        return (value > -1000 && value < 1000);
    }

}
