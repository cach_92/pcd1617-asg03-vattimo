package pcd.ass03;

public interface TemperatureSensor {

    double getCurrentValue();

}
