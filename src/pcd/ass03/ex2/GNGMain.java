package pcd.ass03.ex2;

import io.vertx.core.Vertx;

import java.util.ArrayList;
import java.util.List;

public class GNGMain {

    private static List<Player> players;

    public static void main(final String[] args) {
        int playerNumber = Integer.parseInt(args[0]);

        Vertx vertx = Vertx.vertx();

        Oracle oracle = new Oracle(playerNumber);
        TicketDispenser ticketDispenser = new TicketDispenser(playerNumber);
        players = new ArrayList<>();
        vertx.deployVerticle(oracle, res -> {
            if (res.succeeded()) {
                vertx.deployVerticle(ticketDispenser, res1 -> {
                    if (res.succeeded()) {
//                        System.out.println("Ticket dispenser Deployment id is: " + res1.result());
                        for (int i = 0; i < playerNumber; i++) {
                            Player player = new Player(i);
                            players.add(player);
                            vertx.deployVerticle(player);
                        }
                    } else {
                        System.out.println("Deployment failed!");
                    }
                });
//                System.out.println("Oracle Deployment id is: " + res.result());
            } else {
                System.out.println("Deployment failed!");
            }
        });
    }

}
