package pcd.ass03.ex2;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.MessageConsumer;

import java.util.HashSet;
import java.util.Set;

public class TicketDispenser extends AbstractVerticle {

    private Set<Integer> playerHasTicket;
    private Set<Integer> playerRequestTicket;
    private int ticketNumber;
    private int currentTicketNumber = 1;
    private EventBus eventBus;

    public TicketDispenser(final int ticketNumber) {
        this.playerHasTicket = new HashSet<>();
        this.playerRequestTicket = new HashSet<>();
        this.ticketNumber = ticketNumber;
    }

    @Override
    public void start() throws Exception {
        super.start();
        System.out.println("[TicketDispenser] starting...");
        this.eventBus = this.vertx.eventBus();

        MessageConsumer<String> ticketConsumer = this.eventBus.consumer("dispenser");
        ticketConsumer.handler(message -> {
            int playerId = Integer.parseInt(message.body());
            if (this.playerHasTicket.add(playerId)) {
                this.eventBus.send("player.ticket." + playerId, this.currentTicketNumber++);
                System.out.println("Ticket requesting ... player" + playerId);
            } else {
                this.playerRequestTicket.add(playerId);
                System.out.println("Ticket requesting buffering ... player" + playerId);
            }
            if (this.playerHasTicket.size() == this.ticketNumber) {
                this.changeTurn();
            }
        });

        MessageConsumer<String> gameEndConsumer = this.eventBus.consumer("game.finished");
        gameEndConsumer.handler(message -> this.vertx.undeploy(this.context.deploymentID()));
    }

    private void changeTurn() {
        this.playerHasTicket.clear();
        this.playerRequestTicket.forEach(p -> this.playerHasTicket.add(p));
        this.playerRequestTicket.clear();
        this.currentTicketNumber = 1;
        this.playerHasTicket.forEach(player -> this.eventBus.send("player.ticket." + player.toString(), this.currentTicketNumber++));
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        System.out.println("[TicketDispenser] terminated");
    }
}
