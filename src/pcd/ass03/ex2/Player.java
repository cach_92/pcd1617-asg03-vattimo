package pcd.ass03.ex2;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.MessageConsumer;

public class Player extends AbstractVerticle {

    private EventBus eventBus;
    private int id;
    private long min;
    private long max;
    private long guessValue;
    private boolean won = false;

    public Player(final int id) {
        this.id = id;
        this.min = 0L;
        this.max = Oracle.SEED;
    }

    @Override
    public void start() throws Exception {
        super.start();
        System.out.println("[Player" + this.id + "] starting...");
        this.eventBus = this.vertx.eventBus();

        this.eventBus.publish("dispenser", String.valueOf(this.id));
        MessageConsumer<Integer> ticketConsumer = this.eventBus.consumer("player.ticket." + this.id);
        ticketConsumer.handler(message -> {
            int ticketValue = message.body();
            System.out.println("Player" + this.id + " ticket received: " + ticketValue);
            this.guessValue = (long) (this.min + Math.random() * (this.max - this.min + 1));
            this.eventBus.publish("oracle", this.id + ":" + ticketValue + ":" + this.guessValue);
        });

        MessageConsumer<Integer> guessConsumer = this.eventBus.consumer("player.guess.response." + this.id);
        guessConsumer.handler(message -> {
            int response = message.body();
            if (response == Result.LESS.getValue()) {
                this.max = this.guessValue;
                this.eventBus.publish("dispenser", String.valueOf(this.id));
            } else if (response == Result.GREATER.getValue()) {
                this.min = this.guessValue;
                this.eventBus.publish("dispenser", String.valueOf(this.id));
            } else if (response == Result.FOUND.getValue()) {
                this.won = true;
            }
        });

        MessageConsumer<String> gameEndConsumer = this.eventBus.consumer("game.finished");
        gameEndConsumer.handler(message -> this.vertx.undeploy(this.context.deploymentID()));
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        if (this.won) {
            System.out.println("[Player" + this.id + "] WON!");
        } else {
            System.out.println("[Player" + this.id + "] SOB!");
        }
    }

}
