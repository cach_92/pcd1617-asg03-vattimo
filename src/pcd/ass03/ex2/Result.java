package pcd.ass03.ex2;

public enum Result {

    FOUND(0),
    GREATER(1),
    LESS(-1);

    private final int value;

    Result(final int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

}
