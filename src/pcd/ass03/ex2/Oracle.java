package pcd.ass03.ex2;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.MessageConsumer;

import java.util.HashMap;
import java.util.Map;

public class Oracle extends AbstractVerticle {

    public static final long SEED = 103252520L;
    private EventBus eventBus;
    private long secretNumber;
    private int currentTicketNumber = 1;
    private int playerNumber;
    private Map<Integer, String> messageBuffer;
    private boolean finished = false;

    public Oracle(final int playerNumber) {
        this.playerNumber = playerNumber;
        this.messageBuffer = new HashMap<>();
    }

    @Override
    public void start() throws Exception {
        super.start();
        this.eventBus = this.vertx.eventBus();
        this.secretNumber = (long) (Math.random() * SEED);
        System.out.println("[Oracle] starting... secret number: " + this.secretNumber);

        MessageConsumer<String> guessConsumer = this.eventBus.consumer("oracle");
        guessConsumer.handler(message -> tryToGuess(message.body()));
    }

    private void tryToGuess(final String message) {
        System.out.println("try to guess.. " + this.finished);
        int ticket = getTicket(message);
        if (!this.finished && processMessage(message)) {
            processBuffer();
        } else {
            this.messageBuffer.put(ticket, message);
        }
    }

    private void processBuffer() {
        while (!this.finished && this.messageBuffer.containsKey(this.currentTicketNumber)) {
            String message = this.messageBuffer.get(this.currentTicketNumber);
            this.messageBuffer.remove(this.currentTicketNumber);
            processMessage(message);
        }
    }

    private boolean processMessage(final String message) {
        String[] m = message.split(":");
        int ticket = getTicket(message);
        if (ticket == this.currentTicketNumber) {
            long guessedValue = Long.parseLong(m[2].trim());
            int playerId = Integer.parseInt(m[0].trim());
            System.out.println("Player" + playerId + " Guessing ... " + guessedValue + " with ticket: " + ticket);
            long diff = this.secretNumber - guessedValue;
            if (diff > 0) {
                this.eventBus.send("player.guess.response." + playerId, Result.GREATER.getValue());
            } else if (diff < 0) {
                this.eventBus.send("player.guess.response." + playerId, Result.LESS.getValue());
            } else {
                this.finished = true;
                System.out.println("process ... " + this.finished);
                this.eventBus.send("player.guess.response." + playerId, Result.FOUND.getValue());
                this.eventBus.publish("game.finished", "finish!");
                this.vertx.undeploy(this.context.deploymentID());
            }
            updateTicket();
            return true;
        }
        return false;
    }

    private void updateTicket() {
        this.currentTicketNumber = (this.currentTicketNumber) % this.playerNumber + 1;
    }

    private int getTicket(final String message) {
        String[] m = message.split(":");
        return Integer.parseInt(m[1].trim());
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        System.out.println("[Oracle] terminated");
    }
}
