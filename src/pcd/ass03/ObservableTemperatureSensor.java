package pcd.ass03;

import io.reactivex.Flowable;
import pcd.ass03.TemperatureSensor;

public interface ObservableTemperatureSensor extends TemperatureSensor {
	
	Flowable<Double> createObservable();
	
}
