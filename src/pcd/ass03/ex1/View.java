package pcd.ass03.ex1;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.Terminated;
import akka.actor.UntypedAbstractActor;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class View extends UntypedAbstractActor {

    private ActorRef oracle;
    private ActorRef ticketDispenser;
    private List<ActorRef> players;
    private int counterTerminated = 0;
    private JButton startButton = new JButton("start");
    private JButton stopButton = new JButton("stop");
    private JTextField winnerField;
    private JTextField turnField;

    public View(final int playerNumber) {
        this.players = new ArrayList<>();
        JFrame frame = new JFrame();
        frame.setResizable(false);
        JPanel panel = new JPanel();
        panel.add(this.startButton);
        panel.add(this.stopButton);
        panel.add(new JLabel("Current turn:"));
        this.turnField = new JTextField("0");
        this.turnField.setEnabled(false);
        this.turnField.setColumns(3);
        panel.add(this.turnField);
        panel.add(new JLabel("Winner player:"));
        this.winnerField = new JTextField("-");
        this.winnerField.setEnabled(false);
        this.winnerField.setColumns(3);
        panel.add(this.winnerField);
        frame.getContentPane().add(panel);
        frame.setVisible(true);
        frame.pack();

        this.startButton.addActionListener(l -> {
            this.players.forEach(p -> {
                p.tell(StartGameMessage$.MODULE$, getSelf());
                context().watch(p);
            });
        });

        this.stopButton.addActionListener(l -> {
            this.oracle.tell(GameFinishedMessage$.MODULE$, getSelf());
        });

        this.oracle = getContext().getSystem().actorOf(Props.create(Oracle.class, playerNumber), "Oracle");
        this.ticketDispenser = getContext().getSystem().actorOf(Props.create(TicketDispenser.class, playerNumber), "TicketDispenser");
        for (int i = 1; i <= playerNumber; i++) {
            this.players.add(getContext().getSystem().actorOf(Props.create(Player.class, i, this.ticketDispenser, this.oracle), "Player" + i));
        }
    }

    @Override
    public void onReceive(Object message) throws Throwable {
        if (message instanceof Terminated) {
            this.counterTerminated++;
            if (this.counterTerminated == this.players.size()) {
                getContext().getSystem().stop(this.oracle);
                getContext().getSystem().stop(this.ticketDispenser);

                this.startButton.setEnabled(false);
                this.stopButton.setEnabled(false);
            }
        } else if (message instanceof WinnerMessage) {
            WinnerMessage wm = (WinnerMessage) message;
            this.winnerField.setText(String.valueOf(wm.copy$default$1()));
        } else if (message instanceof TurnMessage) {
            TurnMessage tm = (TurnMessage) message;
            this.turnField.setText(String.valueOf(tm.copy$default$1()));
        }
    }

}
