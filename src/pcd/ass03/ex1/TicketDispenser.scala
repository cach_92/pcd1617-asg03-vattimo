package pcd.ass03.ex1

import akka.actor.{Actor, Stash}

import scala.collection.mutable.ArrayBuffer

case class TicketDispenser(private val playerNumber: Int) extends Actor with Stash {

    private var hasTicket: ArrayBuffer[Boolean] = ArrayBuffer.fill(playerNumber)(false)
    private var currentValue = 1

    override def receive: Receive = {
        case TicketRequestMessage(id) => {
            hasTicket(id - 1) match {
                case true => {
                    if (hasTicket reduce (_ && _)) {
                        reset
                        sender ! TicketMessage(currentValue)
                        hasTicket update(id - 1, true)
                        updateTicket
                    } else {
                        stash
                    }
                }
                case false => {
                    sender ! TicketMessage(currentValue)
                    hasTicket update(id - 1, true)
                    updateTicket
                }
            }
        }
        case _ => unhandled(_)
    }

    def updateTicket: Unit = {
        currentValue match {
            case `playerNumber` => currentValue = 1
            case _ => currentValue = currentValue + 1
        }
    }

    def reset: Unit = {
        hasTicket = ArrayBuffer.fill(playerNumber)(false)
        currentValue = 1
        unstashAll
    }

}