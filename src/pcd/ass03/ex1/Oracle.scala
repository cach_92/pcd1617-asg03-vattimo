package pcd.ass03.ex1

import akka.actor.Actor

import scala.collection.mutable.{Map => MutableMap}

case class Oracle(private val playerNumber: Int) extends Actor {

    private val printer = context actorSelection "../View"
    private val messageBuffer = MutableMap.empty[Int, Message]
    private val secretNumber: Long = (Math.random() * Oracle.SEED).asInstanceOf[Long]
    private var currentTicketNumber = 1
    private var currentTurn = 1

    println(s"[ ${self.path.name} ] - ${secretNumber}")

    override def receive: Receive = {
        case GameFinishedMessage => {
            emptyBuffer
            context become gameEnded
        }
        case RichGuessMessage(ticket, value, src) => {
            (ticket == currentTicketNumber) match {
                case true => {
                    println(s"[ ${src.path.name} ] with ticket: ${ticket} guessed ${value}")
                    (secretNumber - value) match {
                        case x if x > 0 => src ! ResultMessage(ResultValue.GREATER)
                        case x if x < 0 => src ! ResultMessage(ResultValue.LESS)
                        case 0 => {
                            src ! ResultMessage(ResultValue.FOUND)
                            emptyBuffer
                            context become gameEnded
                        }
                    }
                    updateTicket
                    processBuffer
                }
                case false => messageBuffer put(ticket, RichGuessMessage(ticket, value, src))
            }
        }
        case SimpleGuessMessage(ticket, value) => {
            (ticket == currentTicketNumber) match {
                case true => {
                    println(s"[ ${sender.path.name} ] with ticket: ${ticket} guessed ${value}")
                    (secretNumber - value) match {
                        case x if x > 0 => sender ! ResultMessage(ResultValue.GREATER)
                        case x if x < 0 => sender ! ResultMessage(ResultValue.LESS)
                        case 0 => {
                            sender ! ResultMessage(ResultValue.FOUND)
                            emptyBuffer
                            context become gameEnded
                        }
                    }
                    updateTicket
                    processBuffer
                }
                case false => messageBuffer put(ticket, RichGuessMessage(ticket, value, sender))
            }
        }
        case _ => unhandled(_)
    }

    def processBuffer: Unit = {
        var tmpTicket = currentTicketNumber
        while (messageBuffer contains tmpTicket) {
            messageBuffer remove (tmpTicket) foreach (self ! _)
            tmpTicket match {
                case `playerNumber` => {
                    tmpTicket = 1
                    currentTurn += 1
                    printer ! TurnMessage(currentTurn)
                }
                case _ => tmpTicket += 1
            }
        }
    }

    def updateTicket: Unit = {
        currentTicketNumber match {
            case `playerNumber` => {
                currentTicketNumber = 1
                currentTurn += 1
                printer ! TurnMessage(currentTurn)
            }
            case _ => currentTicketNumber += 1
        }
    }

    def gameEnded: Receive = {
        case RichGuessMessage(_, _, s) => s ! GameFinishedMessage
        case _ => sender ! GameFinishedMessage
    }

    def emptyBuffer: Unit = messageBuffer foreach { case (_, RichGuessMessage(_, _, s)) => s ! GameFinishedMessage }

}

object Oracle {
    val SEED: Long = 2472476839257903275L
}
