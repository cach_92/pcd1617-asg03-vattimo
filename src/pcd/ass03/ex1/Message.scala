package pcd.ass03.ex1

import akka.actor.ActorRef

sealed trait Message

sealed trait GuessMessage extends Message {
    def ticket: Int

    def value: Long
}

case class SimpleGuessMessage(override val ticket: Int, override val value: Long) extends GuessMessage

case class RichGuessMessage(override val ticket: Int, override val value: Long, private val src: ActorRef) extends GuessMessage

case class TicketMessage(private val ticketValue: Int) extends Message

case class TicketRequestMessage(private val playerId: Int) extends Message

case class GameFinishedMessage() extends Message

object ResultValue extends Enumeration {
    val FOUND = 0
    val GREATER = 1
    val LESS = -1
}

case class ResultMessage(private val result: Int) extends Message

case class StartGameMessage() extends Message

case class WinnerMessage(private val id: Int) extends Message

case class TurnMessage(private val turn: Int) extends Message