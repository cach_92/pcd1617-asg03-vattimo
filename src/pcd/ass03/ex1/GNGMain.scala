package pcd.ass03.ex1

import akka.actor.{ActorSystem, Props}

object GNGMain extends App {

    override def main(args: Array[String]): Unit = {
        val system = ActorSystem("GNGSystem")
        val playerNumber = args(0).toInt
        system actorOf(Props(new View(playerNumber)), "View")
    }

}
