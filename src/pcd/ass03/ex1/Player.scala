package pcd.ass03.ex1

import akka.actor.{Actor, ActorRef}

case class Player(private val id: Int, private val dispenser: ActorRef, private val oracle: ActorRef) extends Actor {

    private var printer: ActorRef = null
    private var min = 0L
    private var max = Oracle.SEED
    private var guessValue: Long = guessValue(min, max)

    override def receive: Receive = {
        case StartGameMessage => {
            printer = sender
            askForTicket
        }
        case GameFinishedMessage => {
            println(s"[ ${self.path.name} ] SOB!")
            context stop self
        }
        case ResultMessage(x) => x match {
            case ResultValue.FOUND => {
                println(s"[ ${self.path.name} ] WON!")
                printer ! WinnerMessage(id)
                context stop self
            }
            case ResultValue.LESS => {
                max = guessValue
                askForTicket
            }
            case ResultValue.GREATER => {
                min = guessValue
                askForTicket
            }
            case _ => throw new RuntimeException("Result value not correct")
        }
        case TicketMessage(tv) => {
            //            println(id + " : " + tv)
            guessValue = guessValue(min, max)
            oracle ! SimpleGuessMessage(tv, guessValue)
        }
        case _ => unhandled(_)
    }

    def guessValue(min: Long, max: Long): Long = (min + Math.random() * (max - min + 1)).asInstanceOf[Long]

    def askForTicket: Unit = dispenser ! TicketRequestMessage(id)

}
