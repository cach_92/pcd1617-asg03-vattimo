name := "Assignment03-Ricci"

version := "1.0"

scalaVersion := "2.12.2"

val akka = "com.typesafe.akka" %% "akka-actor" % "2.5.1"
val vertx = "io.vertx" %% "vertx-lang-scala" % "3.4.1"
val reactivex = "io.reactivex.rxjava2" % "rxjava" % "2.1.0"

libraryDependencies ++= Seq(akka, vertx, reactivex)

scalaSource in Compile := {
    (baseDirectory in Compile) (_ /"src")
}.value
        